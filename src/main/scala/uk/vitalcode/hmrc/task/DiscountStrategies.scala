package uk.vitalcode.hmrc.task

object DiscountStrategies {
    type DiscountStrategy = Seq[Product] => Double

    def discountOranges: DiscountStrategy = (basket: Seq[Product]) => groupDiscount(basket.filter {
        case Orange => true
        case _ => false
    }, 3, Orange.cost)

    def discountApples: DiscountStrategy = (basket: Seq[Product])  => groupDiscount(basket.filter {
        case Apple => true
        case _ => false
    }, 2, Apple.cost)

    private def groupDiscount[T <: Product](basket: Seq[T], group: Int, cost: Double): Double = {
        basket.sliding(group, group).count(_.size == group) * cost
    }
}
