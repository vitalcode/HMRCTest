package uk.vitalcode.hmrc.task

import uk.vitalcode.hmrc.task.DiscountStrategies._

object Basket {
    def cost(products: Seq[Product]): Double = products.map(_.cost).sum

    def cost(products: Seq[Product], discountStrategies: Seq[DiscountStrategy]): Double = {
        cost(products) - discountStrategies.map(_(products)).sum
    }
}

