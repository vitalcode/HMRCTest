package uk.vitalcode.hmrc.task

trait Product {
    def cost: Double
}

case object Orange extends Product {
    override val cost = 0.25
}

case object Apple extends Product {
    override val cost = 0.60
}
