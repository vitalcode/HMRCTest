package uk.vitalcode.hmrc.task.test

import org.scalatest._
import uk.vitalcode.hmrc.task.{Apple, Basket, Orange}
import uk.vitalcode.hmrc.task.DiscountStrategies._

class BasketTest extends WordSpec with ShouldMatchers {
    "Basket" when {
        "given collection of products" should {
            "cost the products correctly" in {
                val products = Seq(Apple, Apple, Orange, Apple)
                Basket.cost(products) shouldBe 2.05
            }
            "apply correct discount to Apples" in {
                val products = Seq(Apple, Apple, Apple, Apple, Apple)
                val discounts = Seq(discountApples)
                Basket.cost(products, discounts) shouldBe 1.8
            }
            "apply correct discount to Oranges" in {
                val products = Seq(Orange, Orange, Orange, Orange)
                val discounts = Seq(discountOranges)
                Basket.cost(products, discounts) shouldBe 0.75
            }
            "apply correct discount to both Apples and Oranges" in {
                val products = Seq(Apple, Apple, Apple, Orange, Orange, Orange, Apple, Apple, Apple, Orange, Orange)
                val discounts = Seq(discountApples, discountOranges)
                Basket.cost(products, discounts) shouldBe 2.8
            }
        }
    }
}
